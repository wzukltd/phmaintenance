<?php

if (php_sapi_name() != 'cli')
{
  if (strpos($_SERVER['REQUEST_URI'], 'autohealinghealthcheck') !== false)
  {
    exit('Healthy!');
  }
}

if (isset($_GET['phmaintenance']) || is_file('/tmp/phmaintenance'))
{
  if (!getenv('IGNORE_MAINTENANCE'))
  {
    if (php_sapi_name() == 'cli')
    {
      echo "Maintenance mode enabled, delete file at /tmp/phmaintenance to revert"
        . PHP_EOL;
    }
    else
    {
      if (stristr($_SERVER['REQUEST_URI'], 'healthcheck'))
      {
        echo 'Healthy!';
      }
      elseif (stristr($_SERVER['SERVER_NAME'], 'api.'))
      {
        header('Content-Type: application/json');
        echo json_encode(['status' => 'maintenance']);
      }
      else
      {
        ?>
        <style type="text/css">
          body {
            background-color: #dddddd;
          }

          @media all and (min-width: 600px) {
            #message {
              background-color: #fff;
              position:         absolute;
              top:              50%;
              left:             50%;
              width:            600px;
              height:           130px;
              margin:           -65px 0 0 -300px;
              text-align:       center;
              border-radius:    10px;
              box-shadow:       5px 5px 2px #888888;
            }
          }
        </style>

        <div id="message">
          <h1>Sorry, we are performing maintenance.</h1>

          <h3>Please try again shortly. Thank you for understanding.</h3>
        </div>
        <?php
      }
    }

    die();
  }
}
